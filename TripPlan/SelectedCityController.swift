//
//  SelectedCityController.swift
//  TripPlan
//
//  Created by Aung Khant Thet Naing on 1/26/16.
//  Copyright © 2016 Aung Khant Thet Naing. All rights reserved.
//

import UIKit

class SelectedCityController: UIViewController {
    @IBOutlet var bgImgView:UIImageView!
    @IBOutlet var label:UILabel!
    var blurEffectView:UIVisualEffectView?
    var trips:Attraction!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        label.text = trips.type
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView?.frame = view.bounds
    }
}
