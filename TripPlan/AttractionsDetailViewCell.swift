//
//  AttractionsDetailViewCell.swift
//  TripPlan
//
//  Created by Aung Khant Thet Naing on 1/27/16.
//  Copyright © 2016 Aung Khant Thet Naing. All rights reserved.
//

import UIKit

class AttractionsDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var fieldLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
}
