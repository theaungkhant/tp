//
//  AttractionsDetailViewController.swift
//  TripPlan
//
//  Created by Aung Khant Thet Naing on 1/27/16.
//  Copyright © 2016 Aung Khant Thet Naing. All rights reserved.
//

import UIKit

class AttractionsDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    @IBOutlet weak var attractionDetailImg: PFImageView!
    @IBOutlet var tableView:UITableView!
    var currentObject : PFObject?
    var attractions:Attraction!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.estimatedRowHeight = 36.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        if let object = attractions {
            title = attractions.name
            
            let initialThumbnail = UIImage(named: "question")
            attractionDetailImg.image = initialThumbnail
            if let thumbnail = attractions.featuredImage {
                attractionDetailImg.file = thumbnail
                attractionDetailImg.loadInBackground()
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! AttractionsDetailTableViewCell!
        if cell == nil {
            cell = AttractionsDetailTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        }
        
        switch indexPath.row {
            
        case 0:
            cell.fieldLabel.text = "Name"
            cell.valueLabel.text = attractions.name
            
            
        case 1:
            cell.fieldLabel.text = "Type"
            cell.valueLabel.text = attractions.type
            
            
        case 2:
            cell.fieldLabel.text = "Location"
            cell.valueLabel.text = attractions.location
            
        case 3:
            cell.fieldLabel.text = "Description"
            cell.valueLabel.text = attractions.description
            
            
        default:
            
            cell.fieldLabel.text = ""
            cell.valueLabel.text = ""
            
        }
        cell.backgroundColor = UIColor.clearColor()
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showMap" {
            let destinationController = segue.destinationViewController as! MapViewController
            destinationController.att =  attractions
        }
    }
    /*
    @IBAction func addTripPlan(sender: AnyObject) {
            
        let user = PFUser.currentUser()
        //let user = PFObject(className: "User")
        //I am not sure whether to use PFUser.current or PFObject for user.
        
        
        let place = PFObject(className: "places")
        place = attractions.attractionId
        //How to get objectId to save particular place.
        
        let relation = user.relationForKey("user_places")
        relation.addObject(place)
        
        user!.saveInBackground()
        self.navigationController?.popViewControllerAnimated(true)
            
        }
        
    }*/
    
    
    
    
    
}
