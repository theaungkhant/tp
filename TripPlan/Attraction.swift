//
//  Attraction.swift
//  TripPlan
//
//  Created by Aung Khant Thet Naing on 1/25/16.
//  Copyright © 2016 Aung Khant Thet Naing. All rights reserved.
//

import Foundation
import UIKit

class Attraction {
    var attractionId = ""
    var city = ""
    var name = ""
    var type = ""
    var featuredImage:PFFile?
    var description = ""
    var location = ""
    
    
    init(tripId: String, city: String, name: String, type: String, featuredImage: PFFile!, description: String, location: String) {
        self.attractionId = tripId
        self.city = city
        self.name = name
        self.type = type
        self.featuredImage = featuredImage
        self.description = description
        self.location = location
    }
    
    init(pfObject: PFObject) {
        self.attractionId = pfObject.objectId!
        self.city = pfObject["city"] as! String
        self.name = pfObject["name"] as! String
        self.type = pfObject["type"] as! String
        self.featuredImage = pfObject["image"] as? PFFile
        self.description = pfObject["description"] as! String
        self.location = pfObject["location"] as! String
    }
    
    func toPFObject() -> PFObject {
        let tripObject = PFObject(className: "attractions")
        tripObject.objectId = attractionId
        tripObject["city"] = city
        tripObject["name"] = name
        tripObject["type"] = type
        tripObject["featuredImage"] = featuredImage
        tripObject["description"] = description
        tripObject["location"] = location
        
        return tripObject
    }
}